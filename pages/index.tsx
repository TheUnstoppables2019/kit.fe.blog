import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import configuration, { homeColorTheme } from '../config/apis';
import ImgContainer from '../components/ImgContainer/ImgContainer';
// @ts-ignore
import styles from '../styles/Home.module.scss';
import TextContainer from '../components/TextContainer/TextContainer';
import Menu from '../components/Menu/Menu';
import Wave from '../components/Wave/Wave';
import Category from '../components/Category/Category';

const Home: NextPage = () => {
  let i = 1;
  return (
    <div>
      <Head>
        <title>Kitman Yiu | Skills ©</title>
        <meta
          name="description"
          content="React, Node, JAVA and more. A details look of Kitman skills"
        />
        <meta
          property="og:title"
          content="Kitman Yiu | Skills | Home"
          key="title"
        />
        <meta
          property="og:description"
          content="React, Node, JAVA and more. A details look of Kitman skills"
        />
        <meta
          name="twitter:title"
          content="React, Node, JAVA and more. A details look of Kitman skills"
        />
        <meta
          name="twitter:description"
          content="React, Node, JAVA and more. A details look of Kitman skills"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Menu />

        <section className={styles.bg}>
          <div className="shape shape-style-1 shape-primary">
            <span className="circle-150" />
            <span className="circle-50 delay-2s" />
            <span className="circle-50" />
            <span className="circle-75" />
            <span className="circle-100 delay-2s" />
            <span className="circle-75" />
            <span className="circle-50 delay-5s" />
            <span className="circle-100 delay-1s" />
            <span className="circle-50 delay-1s" />
            <span className="circle-100 delay-5s" />
          </div>
          <div className={styles.header__container}>
            <div>
              <h1 className={styles.header__title}>
                React | Node | JAVA | AWS
              </h1>
              <p className={styles.header__text}>
                Love of learning is the most necessary passion ... in it lies
                our happiness. It&#39;s a sure remedy for what ails us, an
                unending source of pleasure—Emilie du Chatelet
              </p>
            </div>
          </div>

          <div className={styles.section__shape}>
            <Image
              src="https://cssmaterial.com/apetech/demo/img/waves-shape.svg"
              alt="dec"
              width={1903}
              height={1070}
            />
          </div>
        </section>
        <section className="">
          <h2 className="color--black text-center mt-150">
            Most Important Skills for Developers
          </h2>
          <div className={styles.home__category}>
            <Category />
          </div>
        </section>
        {configuration.categories.map((item: any) => {
          i *= -1;
          const color: any = item.themeColor ? item.themeColor : '';
          const bgColor = homeColorTheme[color]?.bgColor;
          const title = `${item.slug} HighLights`;
          if (i === 1) {
            return (
              <>
                <Wave bgColor={bgColor} title={title} />
                <div className={styles.section__fe}>
                  <ImgContainer />
                  <TextContainer item={item} />
                </div>
              </>
            );
          }
          return (
            <>
              <Wave bgColor={bgColor} title={title} />
              <div className={styles.section__fe}>
                <TextContainer item={item} />
                <ImgContainer />
              </div>
            </>
          );
        })}
      </main>
      <footer className={styles.footer}>
        <p>© Copyright 0101001 By Kitman</p>
      </footer>
    </div>
  );
};

export default Home;
