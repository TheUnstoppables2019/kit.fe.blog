import {  getPost } from "@/api/kitmanyiuapis";
import Menu from "@/components/Menu/Menu";
import Skills from "@/hoc/Skills/Skills";
import style from '../../../styles/Post.module.css';
import Post from "@/hoc/Post/Post";
import { capitalizeFirstLetter } from "@/utils/utils";

export async function getServerSideProps(context:any) {
  const { category, stack, postPage } = context.params;

  const res: any = await getPost(category, stack,postPage);
  return {
    props: {
      postData: res.data,
      category,
      stack,
      post: postPage,
    },
  };
};

interface IPostPageParams {
  postData: any;
  category: string;
  stack:string;
  post:string;
}

const PostPage = (props : IPostPageParams) => {
  const {name, keyPoint, description} = props.postData;  
  const {stack, post,category}  = props;
  return (
    <div >
      <Menu classes={style.menuClass} />
        <Skills title={category} classes={'card__post'}>
        <Post
              title={name || capitalizeFirstLetter(post).replace('-', ' ')}
              classes="card__post flex"
            >
            <ul>
            {keyPoint.map((item:any) => {
              return <li key={item}>{item}</li>
            })}
            </ul>
            <div dangerouslySetInnerHTML={{ __html: description }} />
            </Post>
        </Skills>
    </div>)
};
  
export default PostPage;


