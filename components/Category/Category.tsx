import Link from 'next/link';
import configuration, { homeColorTheme } from '../../config/apis';
import styles from './Category.module.scss';
import CanvasTri, { COLORS } from '../CanvasTri/CanvasTri';

export const Category = () => (
  <>
    {configuration.categories.map((item: any, index) => {
      const color: any = item.themeColor ? item.themeColor : '';
      return (
        <Link href={`/${item.slug}`} passHref key={`${item.slug}-header`}>
          <div
            className={[styles.bg__card, homeColorTheme[color]?.bgColor].join(
              ' '
            )}
          >
            <div
              className={[
                styles.header__card,
                homeColorTheme[color]?.priColor,
                homeColorTheme[color]?.bgColorBefore,
              ].join(' ')}
            >
              {item.icon}
              <h1
                className={styles.header}
              >{`< ${item.slug.toUpperCase()} />`}</h1>
              <CanvasTri selectColor={COLORS[index]} delay={index * 1000} />
            </div>
          </div>
        </Link>
      );
    })}
  </>
);

export default Category;
